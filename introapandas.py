#Taller: Crear un documento CSV con 20 registros de los valores de 20 productos

# 1. Crear un dataframe a partir del documento CSV
# 2. Imprimir en pantalla la suma del valor de todos los productos
# 3. Imprimir en pantalla el producto con el menor valor
# 4. Imprimir en pantalla una gráfica con los valores de los productos
# 5. Imprimir en pantalla la raíz cúbica de los valores de los primeros 5 productos


import pandas as pd
import matplotlib.pyplot as plt

#1.Dataframe
miDataframe=pd.read_csv('PANDITAS CON ANITA - Hoja 1.csv', encoding="ISO-8859-1")
print(miDataframe)

#2.Suma
print(miDataframe["VALOR"].sum())

#3.Producto con menor valor
print(miDataframe["VALOR"].min())

#4.Gráfica de valores
miDataframe.plot()
plt.show()

#5.Raíz cubica de los primeros 5 productos
print(miDataframe.loc[[0,1,2,3,4]])

